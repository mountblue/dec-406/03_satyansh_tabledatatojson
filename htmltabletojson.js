/* Name : Index.js
 *  Description : Node js script to extract the table out of the html file and convert its data
 *  into json format.
 *  Input : HTML file containing table.
 *  Output : it gives the content of the table into converted json object;
 *  usage : node htmltabletojson.js <filename.html>
 */
//adding file module for reading files
const fs = require('fs');
var x = 0;
var headerlist = [];
var datalist = [];


function extractables(newhtml) {
  var flag = 0;
  for (i = 0; i < newhtml.length; i++) {
    if (newhtml[i].match(/<table.*>/)) {
      flag = 1;
    }
    if (flag == 1) {
      var header = newhtml[i].match(/<th.*>(\w*)<\/th>/);
      if (header) {

        headerlist.push(header[1]);
      }
      var tabledata = newhtml[i].match(/<td.*>(\w*)<\/td>/);

      if (tabledata) {
        datalist.push(tabledata[1]);
      }

    }
    if (newhtml[i].match(/<\/table>/)) {
      flag = 0;
      converttojson();
      datalist = [];
      headerlist = [];
    }
  }

}
//opening read stream to read html file
fs.createReadStream('./index.html').on('data', function(data) {
  var html = data.toString();
  var newhtml = html.split('\n');
  extractables(newhtml);
}).on('err', (err) => {
  console.error(err);
});
//function to convert the extracted header value and data value into json format
function converttojson() {
  var arr = [];
  var temp = {};

  var count = 0;
  for (var i = 0; i < datalist.length; i++) {
    if (count == headerlist.length) {
      count = 0;
      arr.push(JSON.stringify(temp));
      temp = {};
    }

    temp[headerlist[count++]] = datalist[i];
  }
  console.log(arr);
}
